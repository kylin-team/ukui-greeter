HEADERS += \
    $$PWD/keyeventmonitor.h \
    $$PWD/configuration.h \
    $$PWD/monitorwatcher.h

SOURCES += \
    $$PWD/keyeventmonitor.cpp \
    $$PWD/configuration.cpp \
    $$PWD/monitorwatcher.cpp
