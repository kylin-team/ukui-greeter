<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en">
<context>
    <name>BioDeviceView</name>
    <message>
        <source>password login&lt;&lt;</source>
        <translation type="vanished">密码登录&lt;&lt;</translation>
    </message>
    <message>
        <location filename="../bio-verify/biodeviceview.cpp" line="219"/>
        <source>password login</source>
        <translation>密码登录</translation>
    </message>
    <message>
        <location filename="../bio-verify/biodeviceview.cpp" line="225"/>
        <source>fingerprint</source>
        <translation>指纹设备</translation>
    </message>
    <message>
        <location filename="../bio-verify/biodeviceview.cpp" line="228"/>
        <source>fingerevin</source>
        <translation>指静脉设备</translation>
    </message>
    <message>
        <location filename="../bio-verify/biodeviceview.cpp" line="231"/>
        <source>iris</source>
        <translation>虹膜设备</translation>
    </message>
</context>
<context>
    <name>GreeterWindow</name>
    <message>
        <source>Zh</source>
        <translation type="vanished">中</translation>
    </message>
    <message>
        <source>En</source>
        <translation type="vanished">英</translation>
    </message>
    <message>
        <source>Guest</source>
        <translation type="obsolete">游客登录</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="vanished">英语</translation>
    </message>
    <message>
        <source>Chinese</source>
        <translation type="vanished">汉语</translation>
    </message>
    <message>
        <source>zh_CN</source>
        <translation type="obsolete">中</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="206"/>
        <source>CN</source>
        <translation>中</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="209"/>
        <source>EN</source>
        <translation>英</translation>
    </message>
</context>
<context>
    <name>GreeterWrapper</name>
    <message>
        <location filename="../greeter/greeterwrapper.cpp" line="74"/>
        <source>failed to start session.</source>
        <translation>启动会话失败</translation>
    </message>
</context>
<context>
    <name>IconEdit</name>
    <message>
        <location filename="../greeter/iconedit.cpp" line="48"/>
        <source>Login</source>
        <translation>登录</translation>
    </message>
</context>
<context>
    <name>LoginWindow</name>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="244"/>
        <source>logged in</source>
        <translation>已登录</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="434"/>
        <location filename="../greeter/loginwindow.cpp" line="533"/>
        <source>login</source>
        <translation>登录</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="627"/>
        <source>please select the type of authentication</source>
        <translation>请选择认证方式</translation>
    </message>
    <message>
        <source>Please select the type of authentication</source>
        <translation type="vanished">请选择认证方式</translation>
    </message>
    <message>
        <source>Please select the device of authentication</source>
        <translation type="vanished">请选择要进行认证的生物识别设备</translation>
    </message>
    <message>
        <source>Guest</source>
        <translation type="vanished">游客登录</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="529"/>
        <source>Password: </source>
        <translation>密码：</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="531"/>
        <source>login:</source>
        <translation>登录：</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation type="obsolete">密码：</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="584"/>
        <source>Incorrect password, please input again</source>
        <translation>密码错误，请重新输入</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="202"/>
        <location filename="../greeter/loginwindow.cpp" line="462"/>
        <source>Login</source>
        <translation>登录</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">用户名</translation>
    </message>
    <message>
        <source>Failed to start session</source>
        <translation type="obsolete">启动会话失败</translation>
    </message>
    <message>
        <source>password error, please input again</source>
        <translation type="obsolete">密码错误，请重新输入</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../display-switch/ui_mainwindow.h" line="127"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PowerWindow</name>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="67"/>
        <source>Goodbye. Would you like to…</source>
        <translation>再见，你是否想要...</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="70"/>
        <source>Other users are currently logged in to this computer, shutting down now will also close these other sessions.</source>
        <translation>当前已有其他用户登录此电脑，现在关机，其他用户的会话也将被终止。</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="207"/>
        <source>suspend</source>
        <translation>挂起</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="221"/>
        <source>hibernate</source>
        <translation>休眠</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="235"/>
        <source>restart</source>
        <translation>重启</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="249"/>
        <source>shutdown</source>
        <translation>关机</translation>
    </message>
</context>
<context>
    <name>SessionWindow</name>
    <message>
        <location filename="../greeter/sessionwindow.cpp" line="90"/>
        <source>select the desktop environment</source>
        <translation>选择桌面环境</translation>
    </message>
    <message>
        <location filename="../greeter/sessionwindow.cpp" line="127"/>
        <source> (Default)</source>
        <translation> （默认）</translation>
    </message>
</context>
<context>
    <name>UserEntry</name>
    <message>
        <location filename="../greeter/userentry.cpp" line="153"/>
        <source>logged in</source>
        <translation>已登录</translation>
    </message>
</context>
<context>
    <name>UsersModel</name>
    <message>
        <location filename="../greeter/usersmodel.cpp" line="37"/>
        <location filename="../greeter/usersmodel.cpp" line="46"/>
        <source>Guest Session</source>
        <translation>游客登录</translation>
    </message>
    <message>
        <source>Guest</source>
        <translation type="vanished">游客登录</translation>
    </message>
    <message>
        <location filename="../greeter/usersmodel.cpp" line="64"/>
        <location filename="../greeter/usersmodel.cpp" line="72"/>
        <source>Login</source>
        <translation>登录</translation>
    </message>
</context>
</TS>
